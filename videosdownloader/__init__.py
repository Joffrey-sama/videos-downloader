
import json
import re
import sys
import os
import logging
import time
import argparse

import requests

from urllib.parse import urlparse


def download_videos(basename, base_page, sub_page_regex, video_regex, backup_video_regex, headers, cookies):
    logging.info('sub_page_regex : ' + sub_page_regex)
    logging.info('video_regex : ' + video_regex)
    logging.info('backup_video_regex : ' + backup_video_regex)

    base_page_source = requests.get(
        base_page, headers=headers, cookies=cookies)

    if base_page_source.status_code != 200:
        logging.error("Error while fetching base page")
        return

    sub_page_urls = re.findall(sub_page_regex, base_page_source.text)

    if not sub_page_urls:
        logging.error("No sub page found")
        return

    index = 0
    logging.debug("sub_page_urls : " + str(sub_page_urls))

    for sub_page_url in sub_page_urls:
        index += 1
        parsed = urlparse(sub_page_url)
        video_title = str(index) + "-" + \
            os.path.basename(os.path.normpath(parsed.path))
        logging.info("video_title : " + video_title)
        sub_page_source = requests.get(
            sub_page_url, headers=headers, cookies=cookies)

        if sub_page_source.status_code != 200:
            logging.error("Error while fetching sub page " + video_title)
            return

        video_urls = re.findall(video_regex, sub_page_source.text)
        video_index = 0

        if not video_urls:
            logging.warning("No video found for subpage " +
                            video_title + " using default regex, trying backup")
            video_urls = re.findall(backup_video_regex, sub_page_source.text)
            if not video_urls:
                logging.warning("No video found for subpage " +
                                video_title + " using backup regex, skipping")

        logging.debug("video_urls : " + str(video_urls))

        if not os.path.exists("results/"+basename+"/"+video_title):
            os.makedirs("results/"+basename+"/"+video_title)

        for video_url in video_urls:
            video_index += 1
            video_file = requests.get(
                video_url, headers=headers, cookies=cookies)
            if video_file.status_code != 200:
                logging.error('Error while fetching video ' +
                              video_title + "_" + str(video_index))
                return
            open("results/"+basename+'/'+video_title+'/'+basename + '-' + video_title + '(' +
                 str(video_index) + ').mp4', 'wb').write(video_file.content)


def cli():
    parser = argparse.ArgumentParser(
        prog='videos-downloader',
        description='Download videos from a website'
    )
    parser.add_argument(
        "-i", "--input",
        required=True,
        type=argparse.FileType("r"),
        help="input JSON file"
    )
    parser.add_argument(
        '-d', '--debug',
        help="debug log mode",
        action="store_const", dest="loglevel", const=logging.DEBUG,
        default=logging.WARNING
    )
    parser.add_argument(
        '-v', '--verbose',
        help="verbose log mode",
        action="store_const", dest="loglevel", const=logging.INFO
    )
    parser.add_argument(
        "-l", "--logfile",
        required=False,
        help="log file",
        default=None
    )
    
    args = parser.parse_args()
    file = args.input

    logging.basicConfig(filename=args.logfile, level=args.loglevel)
    logging.info('Started')

    data = json.load(file)
    for value in data:
        logging.info("Downloading videos from website " + value['base_page'])
        download_videos(value['basename'], value['base_page'], value['sub_page_regex'],
                        value['video_regex'], value['backup_video_regex'], value['headers'], value['cookies'])
    logging.info('Finished')


if __name__ == "__main__":
    cli()