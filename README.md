# Videos Downloader

Download every videos from a webpage url, a subpage regex and a video url regex.

## 📦 Installation

```
pip install git+https://gitlab.com/Joffrey-sama/videos-downloader.git
```

## 🚀Usage
With a JSON file `values.json` like this:
```json
[
    {
        "base_page": "https://www.example.com/path/to/page",
        "basename": "example",
        "sub_page_regex": "https://www.example.com/subpage/[\\w.,@?^=%&:/~+#-]*[\\w@?^=%&/~+#-]?",
        "video_regex": "'(https://www.example.com/video/[\\w.,@?^=%&:/~+#-]*[\\w@?^=%&/~+#-]?)',\\s*type: 'video/mp4',\\s*label: '720'",
        "backup_video_regex": "https://www.example.com/video/[\\w.,@?^=%&:/~+#-]*[\\w@?^=%&/~+#-]?",
        "headers": {"example": "value"},
        "cookies": {"example": "value"}
    }
]
```
You can simply use the tool with the following command:
```bash
videos-dowloader values.json
```
Dowloaded files will be found inside the 'results' folder in current directory, alongside a log file.

## 🔧 How it works
Search in the website for subpages given the subpage regex, then find every videos urls inside them given the video regex, and download every found videos.
